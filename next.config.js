const { name: APP_NAME, version: APP_VERSION } = require('./public/manifest.json');

/**
  * @type {import('next').NextConfig}
  * */
const config = {
  env: {
    APP_NAME,
    APP_VERSION,
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.sql/,
      use: [
        options.defaultLoaders.babel,
        {
          loader: 'raw-loader',
        },
      ],
    });

    return config;
  },
};

module.exports = config;
