import '../styles/globals.scss';
import type { AppProps /* , AppContext */ } from 'next/app';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { AppLayout } from '@/app-layout';
import { useEffect, useState } from 'react';
import { v4 as uuid } from 'uuid';
import { GraphQLClient } from 'graphql-request';
import * as Realm from 'realm-web';
import Head from 'next/head';
import { AppProvider } from '@/containers/app.container';
import { ReactQueryDevtools } from 'react-query/devtools';
import { faUserShield } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from 'react-bootstrap/Button';
import SSRProvider from 'react-bootstrap/SSRProvider';

function Root({ Component, pageProps }: AppProps): JSX.Element {
  const [queryClient] = useState(() => new QueryClient());
  const [gqlClient, setGqlClient] = useState<GraphQLClient>();
  const [realmApp, setRealmApp] = useState<Realm.App>();
  const [config, setConfig] = useState<{ [key: string]: string }>();
  const [key, setUid] = useState(uuid());
  const [currencyFormatter] = useState(new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }));

  const { noLayout, secure, title } = pageProps || {};

  const login = () => {
    const redirectUrl = `${location.origin}/auth/google`;
    const credentials = Realm.Credentials.google({ redirectUrl });
    realmApp?.logIn(credentials).then((user: Realm.User) => {
      // The logIn() promise will not resolve until you call `handleAuthRedirect()`
      // from the new window after the user has successfully authenticated.
      gqlClient?.setHeader('Authorization', `Bearer ${user.accessToken}`);
      setUid(uuid());
    });
  };

  useEffect(() => {
    fetch('/api/config')
      .then((res) => res.json())
      .then((res) => {
        setConfig(res);

        const app = new Realm.App({ id: res.REALM_APP_ID });
        setRealmApp(app);
        if (app.currentUser) {
          app.currentUser?.refreshAccessToken().then(() => {
            setGqlClient(
              new GraphQLClient(res.GRAPHQL_URL, {
                headers: {
                  Authorization: `Bearer ${app.currentUser?.accessToken}`,
                },
              })
            );
          });
        } else {
          const credentials = Realm.Credentials.anonymous();
          app.logIn(credentials).then((user) => {
            gqlClient?.setHeader('Authorization', `Bearer ${user.accessToken}`);
            setGqlClient(
              new GraphQLClient(res.GRAPHQL_URL, {
                headers: {
                  Authorization: `Bearer ${user.accessToken}`,
                },
              })
            );
          });
        }
      });
  }, []);

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover"
        />

        <title>
          {title ? `${title} • ` : null}
          {config?.APP_NAME}
        </title>
      </Head>

      <SSRProvider>
        <AppProvider key={key} value={{ realmApp, gqlClient, config, currencyFormatter }}>
          {realmApp?.currentUser ? (
            gqlClient && (
              <QueryClientProvider client={queryClient}>
                <Hydrate state={pageProps.dehydratedState}>
                  <AppLayout {...{ noLayout }}>{() => <Component {...pageProps} />}</AppLayout>
                </Hydrate>
                <ReactQueryDevtools toggleButtonProps={{ style: { bottom: '30px', right: 0, left: 'auto' } }} />
              </QueryClientProvider>
            )
          ) : (
            <main className="d-flex justify-content-center align-items-center vh-100 vw-100">
              <Button variant="primary" onClick={login}>
                <FontAwesomeIcon icon={faUserShield} size="4x" fixedWidth />
              </Button>
            </main>
          )}
        </AppProvider>
      </SSRProvider>
    </>
  );
}

export default Root;
