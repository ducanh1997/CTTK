import { pick } from 'lodash';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    res.statusCode = 200;
    res.json(pick(process.env, ['GRAPHQL_URL', 'REALM_APP_ID']));
  } catch (error) {
    res.status(400).json(error);
  }
};
