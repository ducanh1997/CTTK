import { useMemo } from 'react';
// import type { GetServerSideProps } from 'next';
import styled from 'styled-components';
import { Masonry } from 'masonic';
import { gql } from 'graphql-request';
import { useGQL } from '@/components/useGQL';
import Link from 'next/link';
import { ImgLink } from '@/components/img-link';
import { useAppCtx } from '@/containers/app.container';
import keyBy from 'lodash/keyBy';

// type Props = {
// };

const Card = styled.div`
  &:hover {
    border-color: transparent;
    box-shadow: 0 0 0 0.25rem rgb(217 164 6 / 50%);
    color: #000;
  }
`;

const ProductCard = ({ data, width }: any) => {
  const { currencyFormatter } = useAppCtx();

  return (
    <Link href={`/products/${data.slug}`}>
      <Card className="card rounded-3">
        <ImgLink src={data.imageProfile} alt={data.name} className="card-img-top img-fluid" style={{ width }} />
        <div className="card-body">
          {data.name}
          <h4 className="card-title text-warning mb-0">{currencyFormatter.format(data.price || 0)}</h4>
        </div>
      </Card>
    </Link>
  );
};

export default function Home(): JSX.Element {
  const { data, isLoading } = useGQL(
    'products',
    gql`
      {
        products {
          _id
          imageProfile
          name
          slug
          price
        }
        brands {
          _id
          imageProfile
          key
          name
        }
      }
    `
  );

  const brandMap = useMemo(() => {
    return keyBy(data?.brands, (o) => o.key);
  }, [data?.brands]);

  return (
    <div className="container-fluid my-5">
      {!isLoading && (
        <Masonry
          items={data?.products || []}
          columnGutter={24}
          columnWidth={240}
          render={ProductCard}
          className="position-relative"
        />
      )}
    </div>
  );
}

// export const getServerSideProps: GetServerSideProps = async () => {
//   return {
//     props: {
//       manufacturers,
//     },
//   };
// };
