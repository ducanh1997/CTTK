import { GetStaticProps } from 'next';
import { useEffect } from 'react';
import * as Realm from 'realm-web';

export default function Page() {
  useEffect(() => {
    Realm.handleAuthRedirect();
  }, []);

  return null;
}

export const getStaticProps: GetStaticProps = async () => ({
  props: {
    secure: false,
    noLayout: true,
  },
});
