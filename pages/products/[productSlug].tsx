/* eslint-disable react/no-danger */
import { gql } from 'graphql-request';
import Carousel from 'react-bootstrap/Carousel';
import { useGQL } from '@/components/useGQL';
import { useRouter } from 'next/dist/client/router';
import { ImgLink } from '@/components/img-link';
import { useAppCtx } from '@/containers/app.container';
import { useMemo } from 'react';
import keyBy from 'lodash/keyBy';

const MainPage = (): JSX.Element => {
  const { currencyFormatter } = useAppCtx();
  const {
    query: { productSlug },
  } = useRouter();
  const { data, isLoading, isFetched } = useGQL(
    productSlug as string,
    gql`
      {
        product(query: { slug: "$productSlug" }) {
          _id
          name
          description
          images
          providers {
            price
            url
            provider
          }
        }
        providers {
          _id
          imageProfile
          key
          name
        }
      }
    `.replace('$productSlug', productSlug as string)
  );

  const providerMap = useMemo(() => {
    return keyBy(data?.providers, (o) => o.key);
  }, [data?.providers]);

  if (isLoading || !isFetched || !data?.product) return null;

  const { product } = data;

  return (
    <div className="container pt-3">
      <h2 className="mb-5">{product.name}</h2>
      <div className="row">
        <div className="col-md-8">
          <Carousel>
            {product.images?.map((img, inx) => (
              <Carousel.Item className="text-center" key={img}>
                <ImgLink src={img} alt={product.name + inx} className="img-fluid" />
              </Carousel.Item>
            ))}
          </Carousel>
          <div className="mt-4" dangerouslySetInnerHTML={{ __html: product.description }} />
        </div>
        <div className="col-md-4">
          <table className="table table-sm">
            <tbody>
              {product.providers.map((x) => (
                <tr key={x.provider} className="align-middle">
                  <td style={{ maxWidth: '50px' }} className="py-2">
                    <a href={x.url} target="_blank">
                      <ImgLink src={providerMap[x.provider].imageProfile} alt={x.provider} className="img-fluid" />
                    </a>
                  </td>
                  <td className="h3 text-end">
                    <a href={x.url} target="_blank">
                      {currencyFormatter.format(x.price)}
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default MainPage;
