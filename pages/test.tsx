import { useGQL } from '@/components/useGQL';
import { gql } from 'graphql-request';
import { useAppCtx } from '@/containers/app.container';

export default function Home() {
  const { data, isLoading } = useGQL(
    'products',
    gql`
      {
        products {
          _id
          name
          imgProfile
          slug
          createdAt
          createdBy
          updatedAt
          updatedBy
        }
      }
    `
  );
  const { realmApp } = useAppCtx();

  // useEffect(() => {
  //   fetcher(gql`
  //     {
  //       task {
  //         _id
  //         _partition
  //         isComplete
  //         summary
  //       }
  //     }
  //   `);
  // }, []);

  return (
    <div className="container-fluid my-5">
      {!isLoading && (
        <pre>{JSON.stringify({ data, app: realmApp?.currentUser }, null, 4)}</pre>
      )}
    </div>
  );
}
