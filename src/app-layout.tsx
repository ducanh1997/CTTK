import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHouseUser, faSearch } from '@fortawesome/free-solid-svg-icons';
import { MenuDropdown } from './components/header/menu-dropdown';
import { useState } from 'react';
import { Dropdown } from 'react-bootstrap';

const Logo = styled.a`
  background-color: #fff;
  bottom: 0.25rem;
  // box-shadow: 0 0 0 0.25rem rgb(217 164 6 / 50%);
  left: 2rem;
  position: absolute;
`;

const MenuItem = styled.a`
  color: var(--bs-light-yellow) !important;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
  padding-bottom: 0.25rem;
  padding-left: 0 !important;
  padding-right: 0 !important;
  padding-top: 0.25rem;

  &:hover,
  &.active {
    border-bottom: 2px solid var(--bs-warning);
    color: var(--bs-warning) !important;
  }
`;

export function AppLayout({ children: Children }: { children: () => JSX.Element }): JSX.Element {
  const [openTopSearch, setOpenTopSearch] = useState(false);
  console.log(openTopSearch);

  return (
    <main className="flex-grow-1 d-flex flex-column">
      <nav className="navbar navbar-expand-lg navbar-dark bg-white sticky-top">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <ul className="navbar-nav w-100 mb-2 mb-lg-0">
            <li className="nav-item">
              <MenuItem className="nav-link text-uppercase fw-bold active text-center" aria-current="page" href="/">
                <FontAwesomeIcon icon={faHouseUser} fixedWidth size="lg" />
                <br />
                <small>Home</small>
              </MenuItem>
            </li>
            <li className="nav-item form-floating flex-grow-1 ms-3">
              <Dropdown>
                <input
                  id="searcg"
                  className="form-control"
                  placeholder="Tìm kiếm"
                  onClick={() => setOpenTopSearch(true)}
                  onBlur={() => setOpenTopSearch(false)}
                />
                <Dropdown.Menu right className="w-100" show={openTopSearch}>
                  <MenuDropdown />
                </Dropdown.Menu>
              </Dropdown>
            </li>
          </ul>
        </div>
      </nav>
      <MenuDropdown />

      <div className="flex-grow-1">
        <Children />
      </div>

      <footer className="container-fluid bg-warning bg-gradient text-white" style={{ position: 'sticky', bottom: 0 }}>
        <Logo className="p-1 rounded-3" href="#brand">
          <img src="/logo.png" alt="logo" height="50" />
        </Logo>
        <div className="mx-auto text-center py-1">Copyright © CTTK. All Rights Reserved</div>
      </footer>
    </main>
  );
}
