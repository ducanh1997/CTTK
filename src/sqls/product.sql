SELECT
p.id_product,
p.reference,
p.price,
p.date_add,
p.date_upd,
l.description,
    l.description_short,
    l.link_rewrite,
    l.name
FROM ps_product p
    JOIN ps_product_lang l ON p.id_product = l.id_product

