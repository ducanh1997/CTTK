import mariadb from 'mariadb';

export const pool = mariadb.createPool(process.env.DB_CONN);
