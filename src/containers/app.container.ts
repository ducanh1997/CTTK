import { GraphQLClient } from 'graphql-request';
import { createContext, useContext } from 'react';

const AppContainer = createContext<AppCtx>({});

export const AppProvider = AppContainer.Provider;
export function useAppCtx() {
  return useContext(AppContainer);
}

type AppCtx = {
  currencyFormatter?: Intl.NumberFormat;
  gqlClient?: GraphQLClient;
  realmApp?: Realm.App;
  config?: { [key: string]: string };
};
