import { DetailedHTMLProps, FunctionComponent, ImgHTMLAttributes } from 'react';

type Props = DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;

export const ImgLink: FunctionComponent<Props> = ({ src, alt, ...props }: Props) => {
  return <img {...props} alt={alt} src={src || '/no-image.png'} style={{ maxHeight: '400px' }} />;
};
