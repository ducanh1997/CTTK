import { useQuery, UseQueryResult } from 'react-query';
import { useAppCtx } from '@/containers/app.container';
import { Variables, RequestDocument } from 'graphql-request/dist/types';
import { Query } from '@/schema';

export function useGQL<V = Variables, TError = unknown>(
  queryKey: string,
  document: RequestDocument,
  variables?: V
): UseQueryResult<Query, TError> {
  const { gqlClient } = useAppCtx();

  return useQuery(
    queryKey,
    async () => {
      const data = await gqlClient.request<Query>(document, variables);

      return data;
    },
    { refetchOnWindowFocus: false }
  );
}
