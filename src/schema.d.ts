export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  Decimal: any;
  ObjectId: any;
};

export type Brand = {
  __typename?: 'Brand';
  _id?: Maybe<Scalars['ObjectId']>;
  homepage?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  key: Scalars['String'];
  name: Scalars['String'];
};

export type BrandInsertInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  homepage?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  key: Scalars['String'];
  name: Scalars['String'];
};

export type BrandQueryInput = {
  AND?: Maybe<Array<BrandQueryInput>>;
  OR?: Maybe<Array<BrandQueryInput>>;
  _id?: Maybe<Scalars['ObjectId']>;
  _id_exists?: Maybe<Scalars['Boolean']>;
  _id_gt?: Maybe<Scalars['ObjectId']>;
  _id_gte?: Maybe<Scalars['ObjectId']>;
  _id_in?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  _id_lt?: Maybe<Scalars['ObjectId']>;
  _id_lte?: Maybe<Scalars['ObjectId']>;
  _id_ne?: Maybe<Scalars['ObjectId']>;
  _id_nin?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  homepage?: Maybe<Scalars['String']>;
  homepage_exists?: Maybe<Scalars['Boolean']>;
  homepage_gt?: Maybe<Scalars['String']>;
  homepage_gte?: Maybe<Scalars['String']>;
  homepage_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  homepage_lt?: Maybe<Scalars['String']>;
  homepage_lte?: Maybe<Scalars['String']>;
  homepage_ne?: Maybe<Scalars['String']>;
  homepage_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_exists?: Maybe<Scalars['Boolean']>;
  imageProfile_gt?: Maybe<Scalars['String']>;
  imageProfile_gte?: Maybe<Scalars['String']>;
  imageProfile_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile_lt?: Maybe<Scalars['String']>;
  imageProfile_lte?: Maybe<Scalars['String']>;
  imageProfile_ne?: Maybe<Scalars['String']>;
  imageProfile_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  key?: Maybe<Scalars['String']>;
  key_exists?: Maybe<Scalars['Boolean']>;
  key_gt?: Maybe<Scalars['String']>;
  key_gte?: Maybe<Scalars['String']>;
  key_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  key_lt?: Maybe<Scalars['String']>;
  key_lte?: Maybe<Scalars['String']>;
  key_ne?: Maybe<Scalars['String']>;
  key_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  name?: Maybe<Scalars['String']>;
  name_exists?: Maybe<Scalars['Boolean']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_ne?: Maybe<Scalars['String']>;
  name_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export enum BrandSortByInput {
  HomepageAsc = 'HOMEPAGE_ASC',
  HomepageDesc = 'HOMEPAGE_DESC',
  ImageprofileAsc = 'IMAGEPROFILE_ASC',
  ImageprofileDesc = 'IMAGEPROFILE_DESC',
  KeyAsc = 'KEY_ASC',
  KeyDesc = 'KEY_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC',
}

export type BrandUpdateInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  _id_unset?: Maybe<Scalars['Boolean']>;
  homepage?: Maybe<Scalars['String']>;
  homepage_unset?: Maybe<Scalars['Boolean']>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_unset?: Maybe<Scalars['Boolean']>;
  key?: Maybe<Scalars['String']>;
  key_unset?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  name_unset?: Maybe<Scalars['Boolean']>;
};

export type DeleteManyPayload = {
  __typename?: 'DeleteManyPayload';
  deletedCount: Scalars['Int'];
};

export type InsertManyPayload = {
  __typename?: 'InsertManyPayload';
  insertedIds: Array<Maybe<Scalars['ObjectId']>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  deleteManyBrands?: Maybe<DeleteManyPayload>;
  deleteManyProducts?: Maybe<DeleteManyPayload>;
  deleteManyProviders?: Maybe<DeleteManyPayload>;
  deleteOneBrand?: Maybe<Brand>;
  deleteOneProduct?: Maybe<Product>;
  deleteOneProvider?: Maybe<Provider>;
  insertManyBrands?: Maybe<InsertManyPayload>;
  insertManyProducts?: Maybe<InsertManyPayload>;
  insertManyProviders?: Maybe<InsertManyPayload>;
  insertOneBrand?: Maybe<Brand>;
  insertOneProduct?: Maybe<Product>;
  insertOneProvider?: Maybe<Provider>;
  replaceOneBrand?: Maybe<Brand>;
  replaceOneProduct?: Maybe<Product>;
  replaceOneProvider?: Maybe<Provider>;
  updateManyBrands?: Maybe<UpdateManyPayload>;
  updateManyProducts?: Maybe<UpdateManyPayload>;
  updateManyProviders?: Maybe<UpdateManyPayload>;
  updateOneBrand?: Maybe<Brand>;
  updateOneProduct?: Maybe<Product>;
  updateOneProvider?: Maybe<Provider>;
  upsertOneBrand?: Maybe<Brand>;
  upsertOneProduct?: Maybe<Product>;
  upsertOneProvider?: Maybe<Provider>;
};

export type MutationDeleteManyBrandsArgs = {
  query?: Maybe<BrandQueryInput>;
};

export type MutationDeleteManyProductsArgs = {
  query?: Maybe<ProductQueryInput>;
};

export type MutationDeleteManyProvidersArgs = {
  query?: Maybe<ProviderQueryInput>;
};

export type MutationDeleteOneBrandArgs = {
  query: BrandQueryInput;
};

export type MutationDeleteOneProductArgs = {
  query: ProductQueryInput;
};

export type MutationDeleteOneProviderArgs = {
  query: ProviderQueryInput;
};

export type MutationInsertManyBrandsArgs = {
  data: Array<BrandInsertInput>;
};

export type MutationInsertManyProductsArgs = {
  data: Array<ProductInsertInput>;
};

export type MutationInsertManyProvidersArgs = {
  data: Array<ProviderInsertInput>;
};

export type MutationInsertOneBrandArgs = {
  data: BrandInsertInput;
};

export type MutationInsertOneProductArgs = {
  data: ProductInsertInput;
};

export type MutationInsertOneProviderArgs = {
  data: ProviderInsertInput;
};

export type MutationReplaceOneBrandArgs = {
  data: BrandInsertInput;
  query?: Maybe<BrandQueryInput>;
};

export type MutationReplaceOneProductArgs = {
  data: ProductInsertInput;
  query?: Maybe<ProductQueryInput>;
};

export type MutationReplaceOneProviderArgs = {
  data: ProviderInsertInput;
  query?: Maybe<ProviderQueryInput>;
};

export type MutationUpdateManyBrandsArgs = {
  query?: Maybe<BrandQueryInput>;
  set: BrandUpdateInput;
};

export type MutationUpdateManyProductsArgs = {
  query?: Maybe<ProductQueryInput>;
  set: ProductUpdateInput;
};

export type MutationUpdateManyProvidersArgs = {
  query?: Maybe<ProviderQueryInput>;
  set: ProviderUpdateInput;
};

export type MutationUpdateOneBrandArgs = {
  query?: Maybe<BrandQueryInput>;
  set: BrandUpdateInput;
};

export type MutationUpdateOneProductArgs = {
  query?: Maybe<ProductQueryInput>;
  set: ProductUpdateInput;
};

export type MutationUpdateOneProviderArgs = {
  query?: Maybe<ProviderQueryInput>;
  set: ProviderUpdateInput;
};

export type MutationUpsertOneBrandArgs = {
  data: BrandInsertInput;
  query?: Maybe<BrandQueryInput>;
};

export type MutationUpsertOneProductArgs = {
  data: ProductInsertInput;
  query?: Maybe<ProductQueryInput>;
};

export type MutationUpsertOneProviderArgs = {
  data: ProviderInsertInput;
  query?: Maybe<ProviderQueryInput>;
};

export type Product = {
  __typename?: 'Product';
  _id: Scalars['ObjectId'];
  createdAt: Scalars['DateTime'];
  createdUser?: Maybe<User>;
  createdUserId: Scalars['ObjectId'];
  description?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  price?: Maybe<Scalars['Decimal']>;
  providers?: Maybe<Array<Maybe<ProductProvider>>>;
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedUserId?: Maybe<Scalars['ObjectId']>;
};

export type ProductInsertInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  createdAt: Scalars['DateTime'];
  createdUserId: Scalars['ObjectId'];
  description?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  price?: Maybe<Scalars['Decimal']>;
  providers?: Maybe<Array<Maybe<ProductProviderInsertInput>>>;
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedUserId?: Maybe<Scalars['ObjectId']>;
};

export type ProductProvider = {
  __typename?: 'ProductProvider';
  price?: Maybe<Scalars['Decimal']>;
  provider?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export type ProductProviderInsertInput = {
  price?: Maybe<Scalars['Decimal']>;
  provider?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export type ProductProviderQueryInput = {
  AND?: Maybe<Array<ProductProviderQueryInput>>;
  OR?: Maybe<Array<ProductProviderQueryInput>>;
  price?: Maybe<Scalars['Decimal']>;
  price_exists?: Maybe<Scalars['Boolean']>;
  price_gt?: Maybe<Scalars['Decimal']>;
  price_gte?: Maybe<Scalars['Decimal']>;
  price_in?: Maybe<Array<Maybe<Scalars['Decimal']>>>;
  price_lt?: Maybe<Scalars['Decimal']>;
  price_lte?: Maybe<Scalars['Decimal']>;
  price_ne?: Maybe<Scalars['Decimal']>;
  price_nin?: Maybe<Array<Maybe<Scalars['Decimal']>>>;
  provider?: Maybe<Scalars['String']>;
  provider_exists?: Maybe<Scalars['Boolean']>;
  provider_gt?: Maybe<Scalars['String']>;
  provider_gte?: Maybe<Scalars['String']>;
  provider_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  provider_lt?: Maybe<Scalars['String']>;
  provider_lte?: Maybe<Scalars['String']>;
  provider_ne?: Maybe<Scalars['String']>;
  provider_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  url?: Maybe<Scalars['String']>;
  url_exists?: Maybe<Scalars['Boolean']>;
  url_gt?: Maybe<Scalars['String']>;
  url_gte?: Maybe<Scalars['String']>;
  url_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  url_lt?: Maybe<Scalars['String']>;
  url_lte?: Maybe<Scalars['String']>;
  url_ne?: Maybe<Scalars['String']>;
  url_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type ProductProviderUpdateInput = {
  price?: Maybe<Scalars['Decimal']>;
  price_unset?: Maybe<Scalars['Boolean']>;
  provider?: Maybe<Scalars['String']>;
  provider_unset?: Maybe<Scalars['Boolean']>;
  url?: Maybe<Scalars['String']>;
  url_unset?: Maybe<Scalars['Boolean']>;
};

export type ProductQueryInput = {
  AND?: Maybe<Array<ProductQueryInput>>;
  OR?: Maybe<Array<ProductQueryInput>>;
  _id?: Maybe<Scalars['ObjectId']>;
  _id_exists?: Maybe<Scalars['Boolean']>;
  _id_gt?: Maybe<Scalars['ObjectId']>;
  _id_gte?: Maybe<Scalars['ObjectId']>;
  _id_in?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  _id_lt?: Maybe<Scalars['ObjectId']>;
  _id_lte?: Maybe<Scalars['ObjectId']>;
  _id_ne?: Maybe<Scalars['ObjectId']>;
  _id_nin?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdAt_exists?: Maybe<Scalars['Boolean']>;
  createdAt_gt?: Maybe<Scalars['DateTime']>;
  createdAt_gte?: Maybe<Scalars['DateTime']>;
  createdAt_in?: Maybe<Array<Maybe<Scalars['DateTime']>>>;
  createdAt_lt?: Maybe<Scalars['DateTime']>;
  createdAt_lte?: Maybe<Scalars['DateTime']>;
  createdAt_ne?: Maybe<Scalars['DateTime']>;
  createdAt_nin?: Maybe<Array<Maybe<Scalars['DateTime']>>>;
  createdUserId?: Maybe<Scalars['ObjectId']>;
  createdUserId_exists?: Maybe<Scalars['Boolean']>;
  createdUserId_gt?: Maybe<Scalars['ObjectId']>;
  createdUserId_gte?: Maybe<Scalars['ObjectId']>;
  createdUserId_in?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  createdUserId_lt?: Maybe<Scalars['ObjectId']>;
  createdUserId_lte?: Maybe<Scalars['ObjectId']>;
  createdUserId_ne?: Maybe<Scalars['ObjectId']>;
  createdUserId_nin?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  description?: Maybe<Scalars['String']>;
  description_exists?: Maybe<Scalars['Boolean']>;
  description_gt?: Maybe<Scalars['String']>;
  description_gte?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  description_lt?: Maybe<Scalars['String']>;
  description_lte?: Maybe<Scalars['String']>;
  description_ne?: Maybe<Scalars['String']>;
  description_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_exists?: Maybe<Scalars['Boolean']>;
  imageProfile_gt?: Maybe<Scalars['String']>;
  imageProfile_gte?: Maybe<Scalars['String']>;
  imageProfile_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile_lt?: Maybe<Scalars['String']>;
  imageProfile_lte?: Maybe<Scalars['String']>;
  imageProfile_ne?: Maybe<Scalars['String']>;
  imageProfile_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  images_exists?: Maybe<Scalars['Boolean']>;
  images_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  images_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  name?: Maybe<Scalars['String']>;
  name_exists?: Maybe<Scalars['Boolean']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_ne?: Maybe<Scalars['String']>;
  name_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  price?: Maybe<Scalars['Decimal']>;
  price_exists?: Maybe<Scalars['Boolean']>;
  price_gt?: Maybe<Scalars['Decimal']>;
  price_gte?: Maybe<Scalars['Decimal']>;
  price_in?: Maybe<Array<Maybe<Scalars['Decimal']>>>;
  price_lt?: Maybe<Scalars['Decimal']>;
  price_lte?: Maybe<Scalars['Decimal']>;
  price_ne?: Maybe<Scalars['Decimal']>;
  price_nin?: Maybe<Array<Maybe<Scalars['Decimal']>>>;
  providers?: Maybe<Array<Maybe<ProductProviderQueryInput>>>;
  providers_exists?: Maybe<Scalars['Boolean']>;
  providers_in?: Maybe<Array<Maybe<ProductProviderQueryInput>>>;
  providers_nin?: Maybe<Array<Maybe<ProductProviderQueryInput>>>;
  slug?: Maybe<Scalars['String']>;
  slug_exists?: Maybe<Scalars['Boolean']>;
  slug_gt?: Maybe<Scalars['String']>;
  slug_gte?: Maybe<Scalars['String']>;
  slug_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  slug_lt?: Maybe<Scalars['String']>;
  slug_lte?: Maybe<Scalars['String']>;
  slug_ne?: Maybe<Scalars['String']>;
  slug_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedAt_exists?: Maybe<Scalars['Boolean']>;
  updatedAt_gt?: Maybe<Scalars['DateTime']>;
  updatedAt_gte?: Maybe<Scalars['DateTime']>;
  updatedAt_in?: Maybe<Array<Maybe<Scalars['DateTime']>>>;
  updatedAt_lt?: Maybe<Scalars['DateTime']>;
  updatedAt_lte?: Maybe<Scalars['DateTime']>;
  updatedAt_ne?: Maybe<Scalars['DateTime']>;
  updatedAt_nin?: Maybe<Array<Maybe<Scalars['DateTime']>>>;
  updatedUserId?: Maybe<Scalars['ObjectId']>;
  updatedUserId_exists?: Maybe<Scalars['Boolean']>;
  updatedUserId_gt?: Maybe<Scalars['ObjectId']>;
  updatedUserId_gte?: Maybe<Scalars['ObjectId']>;
  updatedUserId_in?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  updatedUserId_lt?: Maybe<Scalars['ObjectId']>;
  updatedUserId_lte?: Maybe<Scalars['ObjectId']>;
  updatedUserId_ne?: Maybe<Scalars['ObjectId']>;
  updatedUserId_nin?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
};

export enum ProductSortByInput {
  CreatedatAsc = 'CREATEDAT_ASC',
  CreatedatDesc = 'CREATEDAT_DESC',
  CreateduseridAsc = 'CREATEDUSERID_ASC',
  CreateduseridDesc = 'CREATEDUSERID_DESC',
  DescriptionAsc = 'DESCRIPTION_ASC',
  DescriptionDesc = 'DESCRIPTION_DESC',
  ImageprofileAsc = 'IMAGEPROFILE_ASC',
  ImageprofileDesc = 'IMAGEPROFILE_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  PriceAsc = 'PRICE_ASC',
  PriceDesc = 'PRICE_DESC',
  SlugAsc = 'SLUG_ASC',
  SlugDesc = 'SLUG_DESC',
  UpdatedatAsc = 'UPDATEDAT_ASC',
  UpdatedatDesc = 'UPDATEDAT_DESC',
  UpdateduseridAsc = 'UPDATEDUSERID_ASC',
  UpdateduseridDesc = 'UPDATEDUSERID_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC',
}

export type ProductUpdateInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  _id_unset?: Maybe<Scalars['Boolean']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdAt_unset?: Maybe<Scalars['Boolean']>;
  createdUserId?: Maybe<Scalars['ObjectId']>;
  createdUserId_unset?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  description_unset?: Maybe<Scalars['Boolean']>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_unset?: Maybe<Scalars['Boolean']>;
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  images_unset?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  name_unset?: Maybe<Scalars['Boolean']>;
  price?: Maybe<Scalars['Decimal']>;
  price_unset?: Maybe<Scalars['Boolean']>;
  providers?: Maybe<Array<Maybe<ProductProviderUpdateInput>>>;
  providers_unset?: Maybe<Scalars['Boolean']>;
  slug?: Maybe<Scalars['String']>;
  slug_unset?: Maybe<Scalars['Boolean']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  updatedAt_unset?: Maybe<Scalars['Boolean']>;
  updatedUserId?: Maybe<Scalars['ObjectId']>;
  updatedUserId_unset?: Maybe<Scalars['Boolean']>;
};

export type Provider = {
  __typename?: 'Provider';
  _id?: Maybe<Scalars['ObjectId']>;
  homepage?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  key: Scalars['String'];
  name: Scalars['String'];
};

export type ProviderInsertInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  homepage?: Maybe<Scalars['String']>;
  imageProfile: Scalars['String'];
  key: Scalars['String'];
  name: Scalars['String'];
};

export type ProviderQueryInput = {
  AND?: Maybe<Array<ProviderQueryInput>>;
  OR?: Maybe<Array<ProviderQueryInput>>;
  _id?: Maybe<Scalars['ObjectId']>;
  _id_exists?: Maybe<Scalars['Boolean']>;
  _id_gt?: Maybe<Scalars['ObjectId']>;
  _id_gte?: Maybe<Scalars['ObjectId']>;
  _id_in?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  _id_lt?: Maybe<Scalars['ObjectId']>;
  _id_lte?: Maybe<Scalars['ObjectId']>;
  _id_ne?: Maybe<Scalars['ObjectId']>;
  _id_nin?: Maybe<Array<Maybe<Scalars['ObjectId']>>>;
  homepage?: Maybe<Scalars['String']>;
  homepage_exists?: Maybe<Scalars['Boolean']>;
  homepage_gt?: Maybe<Scalars['String']>;
  homepage_gte?: Maybe<Scalars['String']>;
  homepage_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  homepage_lt?: Maybe<Scalars['String']>;
  homepage_lte?: Maybe<Scalars['String']>;
  homepage_ne?: Maybe<Scalars['String']>;
  homepage_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_exists?: Maybe<Scalars['Boolean']>;
  imageProfile_gt?: Maybe<Scalars['String']>;
  imageProfile_gte?: Maybe<Scalars['String']>;
  imageProfile_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  imageProfile_lt?: Maybe<Scalars['String']>;
  imageProfile_lte?: Maybe<Scalars['String']>;
  imageProfile_ne?: Maybe<Scalars['String']>;
  imageProfile_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  key?: Maybe<Scalars['String']>;
  key_exists?: Maybe<Scalars['Boolean']>;
  key_gt?: Maybe<Scalars['String']>;
  key_gte?: Maybe<Scalars['String']>;
  key_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  key_lt?: Maybe<Scalars['String']>;
  key_lte?: Maybe<Scalars['String']>;
  key_ne?: Maybe<Scalars['String']>;
  key_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
  name?: Maybe<Scalars['String']>;
  name_exists?: Maybe<Scalars['Boolean']>;
  name_gt?: Maybe<Scalars['String']>;
  name_gte?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Maybe<Scalars['String']>>>;
  name_lt?: Maybe<Scalars['String']>;
  name_lte?: Maybe<Scalars['String']>;
  name_ne?: Maybe<Scalars['String']>;
  name_nin?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export enum ProviderSortByInput {
  HomepageAsc = 'HOMEPAGE_ASC',
  HomepageDesc = 'HOMEPAGE_DESC',
  ImageprofileAsc = 'IMAGEPROFILE_ASC',
  ImageprofileDesc = 'IMAGEPROFILE_DESC',
  KeyAsc = 'KEY_ASC',
  KeyDesc = 'KEY_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  IdAsc = '_ID_ASC',
  IdDesc = '_ID_DESC',
}

export type ProviderUpdateInput = {
  _id?: Maybe<Scalars['ObjectId']>;
  _id_unset?: Maybe<Scalars['Boolean']>;
  homepage?: Maybe<Scalars['String']>;
  homepage_unset?: Maybe<Scalars['Boolean']>;
  imageProfile?: Maybe<Scalars['String']>;
  imageProfile_unset?: Maybe<Scalars['Boolean']>;
  key?: Maybe<Scalars['String']>;
  key_unset?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  name_unset?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  brand?: Maybe<Brand>;
  brands: Array<Maybe<Brand>>;
  product?: Maybe<Product>;
  products: Array<Maybe<Product>>;
  provider?: Maybe<Provider>;
  providers: Array<Maybe<Provider>>;
};

export type QueryBrandArgs = {
  query?: Maybe<BrandQueryInput>;
};

export type QueryBrandsArgs = {
  limit?: Maybe<Scalars['Int']>;
  query?: Maybe<BrandQueryInput>;
  sortBy?: Maybe<BrandSortByInput>;
};

export type QueryProductArgs = {
  query?: Maybe<ProductQueryInput>;
};

export type QueryProductsArgs = {
  limit?: Maybe<Scalars['Int']>;
  query?: Maybe<ProductQueryInput>;
  sortBy?: Maybe<ProductSortByInput>;
};

export type QueryProviderArgs = {
  query?: Maybe<ProviderQueryInput>;
};

export type QueryProvidersArgs = {
  limit?: Maybe<Scalars['Int']>;
  query?: Maybe<ProviderQueryInput>;
  sortBy?: Maybe<ProviderSortByInput>;
};

export type UpdateManyPayload = {
  __typename?: 'UpdateManyPayload';
  matchedCount: Scalars['Int'];
  modifiedCount: Scalars['Int'];
};

export type User = {
  __typename?: 'User';
  name?: Maybe<Scalars['String']>;
};
